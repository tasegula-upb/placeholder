package ro.upb.ims.placeholder.controller;

public class EndpointHosts {

    public static final String FACEBOOK_HOST = "https://graph.facebook.com/v2.8/";
    public static final String INSTAGRAM_HOST = "https://api.instagram.com/v1";

}
