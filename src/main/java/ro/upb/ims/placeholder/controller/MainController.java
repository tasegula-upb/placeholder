package ro.upb.ims.placeholder.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class MainController {

    @Autowired
    OAuth2ClientContext oauth2ClientContext;

    @RequestMapping("/user")
    public String user(OAuth2Authentication principal) {
        Map<String, String> details = (Map<String, String>) principal.getUserAuthentication().getDetails();

        String name = details.get("name");
        String id = details.get("id");
        if (id == null) id = details.get("sub");
        return new Gson().toJson(new UserInfo(name, id));
    }

    private static final class UserInfo {
        public final String name;
        public final String id;

        private UserInfo(String name, String id) {
            this.name = name;
            this.id = id;
        }
    }
}
