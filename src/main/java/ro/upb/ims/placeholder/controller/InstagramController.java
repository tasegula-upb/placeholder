package ro.upb.ims.placeholder.controller;

import com.google.gson.Gson;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ro.upb.ims.placeholder.model.instagram.MediaResponseWrapper;
import ro.upb.ims.placeholder.model.instagram.MediaWrapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
public class InstagramController {
    RestTemplate rest = new RestTemplate();

    @RequestMapping(method = RequestMethod.GET,
            value = "/photos",
            params = {"lat, lng"})
    public String photos(
            @RequestParam double lat,
            @RequestParam double lng,
            OAuth2Authentication user) {
        OAuth2AuthenticationDetails userDetails = (OAuth2AuthenticationDetails) user.getDetails();

        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(EndpointHosts.INSTAGRAM_HOST + "/media/search")
                .queryParam("lat", lat)
                .queryParam("lng", lng)
                .queryParam("distance", 5000)
                .queryParam("access_token", userDetails.getTokenValue());

        MediaResponseWrapper response = rest.getForObject(uri.build().toString(), MediaResponseWrapper.class);

        List<MediaWrapper.Media> data = response.getData().stream()
                .filter(media -> Objects.equals(media.getType(), "image"))
                .map(media -> MediaWrapper.Media.from(media))
                .collect(Collectors.toList());

        MediaWrapper result = new MediaWrapper();
        result.setData(data);

        return new Gson().toJson(result);
    }
}
