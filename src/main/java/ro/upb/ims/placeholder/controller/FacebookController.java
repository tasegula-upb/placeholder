package ro.upb.ims.placeholder.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ro.upb.ims.placeholder.model.fb.*;
import ro.upb.ims.placeholder.model.placeholder.*;
import ro.upb.ims.placeholder.utils.Coordinates;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/facebook")
public class FacebookController {
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
    private static final double LAT_MODIFIER = 0.00005;
    private static final double LNG_MODIFIER = 0.00005;

    private static final SimpleDateFormat SDF = new SimpleDateFormat(DATE_FORMAT);

    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    private static final RestTemplate rest = new RestTemplate();

    @RequestMapping(method = RequestMethod.GET,
            value = "/array")
    public String array(OAuth2Authentication principal) {
        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) principal.getDetails();
        Set<String> existingPhotos = new HashSet<>();
        Map<Coordinates, PhotoCoord> photosMap = new HashMap<>();
        String token = details.getTokenValue();

        UriComponentsBuilder photoUri = UriComponentsBuilder.fromHttpUrl(
                EndpointHosts.FACEBOOK_HOST + principal.getName() + "/photos")
                .queryParam("access_token", token);

        RawPhotosWrapper pw = rest.getForObject(photoUri.build().toString(), RawPhotosWrapper.class);
        List<PhotoLocation> data = getPhotos(pw, token, existingPhotos);

        pw = rest.getForObject(photoUri.queryParam("type", "uploaded").build().toString(), RawPhotosWrapper.class);
        data.addAll(getPhotos(pw, token, existingPhotos));

        for (PhotoLocation photoLocation : data) {
            PhotoCoord photo = photosMap.get(photoLocation.getCoordinates());

            if (photo == null) {
                photo = PhotoCoord.from(photoLocation);
            } else {
                photo.addPhoto(Photo.from(photoLocation));
            }
            photosMap.put(photoLocation.getCoordinates(), photo);
        }

        PhotoCoordWrapper result = new PhotoCoordWrapper(new ArrayList<>(photosMap.values()));
        return gson.toJson(result);
    }

    private static List<PhotoLocation> getPhotos(RawPhotosWrapper pw, String token, Set<String> existingPhotos) {
        List<PhotoLocation> data = processList(pw, token, existingPhotos);

        while (pw.getPaging().getNext() != null) {
            pw = rest.getForObject(pw.getPaging().getNext(), RawPhotosWrapper.class);
            data.addAll(processList(pw, token, existingPhotos));
        }

        return data;
    }

    private static List<PhotoLocation> processList(RawPhotosWrapper pw, String token, Set<String> existingPhotos) {
        return pw.getData()
                .stream()
                .map(photo -> {
                    PlaceWrapper place = rest.getForObject(uriForLocation(token, photo), PlaceWrapper.class);
                    if (existingPhotos.contains(photo.getId())) return null;
                    existingPhotos.add(photo.getId());

                    PhotoLocation pl = new PhotoLocation();
                    pl.setUrl(uriForPicture(token, photo));
                    pl.setContent(photo.getName());

                    try {
                        pl.setCreatedTime(SDF.parse(photo.getCreated_time()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (place == null)
                        return pl.setCoordinates(0, 0);
                    if (place.getPlace() == null)
                        return pl.setCoordinates(40, -40);
                    if (place.getPlace().getLocation() == null)
                        return pl.setCoordinates(40, 5);

                    Location location = place.getPlace().getLocation();
                    pl.setCoordinates(location.getLatitude(), location.getLongitude());
                    return pl;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private static String uriForPicture(String token, RawPhoto photo) {
        return UriComponentsBuilder.fromHttpUrl(
                EndpointHosts.FACEBOOK_HOST + photo.getId() + "/picture")
                .queryParam("type", "normal")
                .queryParam("access_token", token)
                .build().toString();
    }

    private static String uriForLocation(String token, RawPhoto photo) {
        return UriComponentsBuilder.fromHttpUrl(
                EndpointHosts.FACEBOOK_HOST + photo.getId())
                .queryParam("fields", "place")
                .queryParam("access_token", token)
                .build().toString();
    }
}
