package ro.upb.ims.placeholder.model.placeholder;

import ro.upb.ims.placeholder.utils.Coordinates;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoLocation {
    private static final String DATE_FORMAT = "dd MMM yyyy, hh:mm:ss";
    private static final SimpleDateFormat SDF = new SimpleDateFormat(DATE_FORMAT);

    private String url;
    private Coordinates coordinates;

    private String createdTime;
    private String content;

    public String getUrl() {
        return url;
    }

    public PhotoLocation setUrl(String url) {
        this.url = url;
        return this;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public PhotoLocation setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
        return this;
    }

    public PhotoLocation setCoordinates(double lat, double lng) {
        this.coordinates = new Coordinates(lat, lng);
        return this;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public PhotoLocation setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public PhotoLocation setCreatedTime(Date createdTime) {
        this.createdTime = SDF.format(createdTime);
        return this;
    }

    public String getContent() {
        return content;
    }

    public PhotoLocation setContent(String content) {
        this.content = content;
        return this;
    }
}
