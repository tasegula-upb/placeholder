package ro.upb.ims.placeholder.model.instagram;

import java.util.List;

public class MediaWrapper {

    private List<Media> data;

    public List<Media> getData() {
        return data;
    }

    public void setData(List<Media> data) {
        this.data = data;
    }

    public static final class Media {
        private String id;
        private String type;
        private String location;
        private Image image;

        public static Media from(MediaResponseWrapper.Media response) {
            Media media = new Media();
            media.id = response.getId();
            media.type = response.getType();
            media.location = response.getLocation();
            media.setImage(response.getImages());

            return media;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Image getImage() {
            return image;
        }

        public void setImage(Image images) {
            this.image = images;
        }

        public void setImage(ImageWrapper images) {
            this.image = images.getStandardResolution();
        }
    }
}
