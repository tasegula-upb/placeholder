package ro.upb.ims.placeholder.model.placeholder;

import java.util.List;

public class PhotoCoordWrapper {
    private List<PhotoCoord> data;

    public PhotoCoordWrapper(List<PhotoCoord> data) {
        this.data = data;
    }

    public List<PhotoCoord> getData() {
        return data;
    }

    public PhotoCoordWrapper setData(List<PhotoCoord> data) {
        this.data = data;
        return this;
    }
}
