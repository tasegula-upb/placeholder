package ro.upb.ims.placeholder.model.placeholder;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Photo {
    private static final String DATE_FORMAT = "dd MMM yyyy, hh:mm:ss";
    private static final SimpleDateFormat SDF = new SimpleDateFormat(DATE_FORMAT);

    private String url;
    private String createdTime;
    private String content;

    public static Photo from(PhotoLocation pl) {
        return new Photo()
                .setUrl(pl.getUrl())
                .setCreatedTime(pl.getCreatedTime())
                .setContent(pl.getContent());
    }

    public String getUrl() {
        return url;
    }

    public Photo setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public Photo setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public Photo setCreatedTime(Date createdTime) {
        this.createdTime = SDF.format(createdTime);
        return this;
    }

    public String getContent() {
        return content;
    }

    public Photo setContent(String content) {
        if (content != null)
            this.content = content;
        else
            this.content = "";
        return this;
    }
}
