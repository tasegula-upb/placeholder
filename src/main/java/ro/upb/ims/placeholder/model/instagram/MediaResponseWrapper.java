package ro.upb.ims.placeholder.model.instagram;

import java.util.List;

public class MediaResponseWrapper {

    private List<Media> data;

    public List<Media> getData() {
        return data;
    }

    public void setData(List<Media> data) {
        this.data = data;
    }

    public static final class Media {
        private String id;
        private String type;
        private String location;
        private ImageWrapper images;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public ImageWrapper getImages() {
            return images;
        }

        public void setImages(ImageWrapper images) {
            this.images = images;
        }
    }
}
