package ro.upb.ims.placeholder.model.placeholder;

import ro.upb.ims.placeholder.utils.Coordinates;

import java.util.ArrayList;
import java.util.List;

public class PhotoCoord {

    private List<Photo> photos;
    private Coordinates coordinates;

    public PhotoCoord() {
        photos = new ArrayList<>();
    }

    public static PhotoCoord from(PhotoLocation pl) {
        return new PhotoCoord()
                .addPhoto(Photo.from(pl))
                .setCoordinates(pl.getCoordinates());
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public PhotoCoord setPhotos(List<Photo> photos) {
        this.photos = photos;
        return this;
    }

    public PhotoCoord addPhoto(Photo photo) {
        this.photos.add(photo);
        return this;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public PhotoCoord setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
        return this;
    }

    public PhotoCoord setCoordinates(double lat, double lng) {
        this.coordinates = new Coordinates(lat, lng);
        return this;
    }

}
