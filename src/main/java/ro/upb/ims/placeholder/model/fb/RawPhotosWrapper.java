package ro.upb.ims.placeholder.model.fb;

import java.util.List;

public class RawPhotosWrapper {
    private List<RawPhoto> data;
    private RawPhotosPaging paging;

    public List<RawPhoto> getData() {
        return data;
    }

    public void setData(List<RawPhoto> data) {
        this.data = data;
    }

    public RawPhotosPaging getPaging() {
        return paging;
    }

    public void setPaging(RawPhotosPaging paging) {
        this.paging = paging;
    }

}