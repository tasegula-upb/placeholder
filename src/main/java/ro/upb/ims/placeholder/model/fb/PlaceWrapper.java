package ro.upb.ims.placeholder.model.fb;

public class PlaceWrapper {

    private Place place;

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
